#include "recheck.h"

using namespace std;

bool _test::resolve()
{
  if(parent) { parent->resolve(this); }

  return result;
}

/******************************************************************************/
/* UNIT TESTS for TESTING FRAMEWORK                                           */
/******************************************************************************/
void selfTest() {

      printf( TITLE );

      {
        stack("Recheck Sample Test");

        it("should compile and display a successful test");

        expect(1).toBeEqual(1);
      }

      {
        stack( "SupaDupa Function Tests" );

        // we have to put the variable outside the it-expect block or
        // it will fall out of scope
        int* ptr;

        it( "should allocate a new ptr");

          ptr = new int;

        expect(ptr).toNotBeNull();

        it( "should assign a value to the ptr" );

          *ptr = 9;

        expect(*ptr).toBeEqual(9);

        delete ptr;
      }
}

int main() {
        selfTest();
}
