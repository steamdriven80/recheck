# recheck
A C++ unit testing framework that follows in the footsteps of Jasmine for javascript.

Allows simple unit testing to be written alongside your regular code:

```C++
#include "recheck.h"

void supaDupaCode() {

  printf( TITLE );

  {
    stack("Recheck Sample Test");

    it("should compile and display a successful test");

    expect(1).toBeEqual(1);
  }

  {
    stack( "SupaDupa Function Tests" );

    // we have to put the variable outside the it-expect block or
    // it will fall out of scope
    int* ptr;

    it( "should allocate a new ptr");

      ptr = new int;

    expect(ptr).toNotBeNull();

    it( "should assign a value to the ptr" );

      *ptr = 9;

    expect(*ptr).toBeEqual(9);

    delete ptr;
  }
}
```

outputs the following:

```shell
☈echeck Testing Framework
-------------------------
✓  — Recheck Sample Test
   ✓  should compile and display a successful test

✓  — SupaDupa Function Tests
   ✓  should allocate a new ptr
   ✓  should assign a value to the ptr

```

Use it however you want, license is MIT and pull requests welcome!  Cheers.
